aprobot
=======

Bot for Discord that announces the tracks playing from an Airtime or
Airtime Pro streaming radio station.

Rather than fill the channel with constant traffic, aprobot deletes old
announcements.

```discord
[4:30 PM] aprobot: zero-project - Through the looking glass (4m53s), finishes at 2018-06-04 04:34:53
[4:35 PM] aprobot: MASTER BOOT RECORD - SET SOUND=C:\CLASSICAL (3m24s), finishes at 2018-06-04 04:38:24
[4:38 PM] aprobot: Mekajinn - Bohemian Girl (Radio Mix) (3m54s), finishes at 2018-06-04 04:42:18
[4:42 PM] aprobot: Miguel Johnson - The Endless Dark (4m25s), finishes at 2018-06-04 04:46:43
[4:46 PM] aprobot: Merkk - Journey (6m37s), finishes at 2018-06-04 04:53:20
```

Uses the live-info-v2 API to constantly collect track data, see
<https://wiki.sourcefabric.org/display/CC/API+Documentation>

Does not use the Playout History Feed API, as this is available to Airtime
Pro users only.

License
-------

The code for this bot is licensed under the GNU Affero General Public
License, <https://www.gnu.org/licenses/agpl-3.0.en.html>

Versioning
----------

* Version 1.0.0 - 13 June 2018

Using semantic versioning (see <https://semver.org/> for details).
The public API is the combination of command-line flags plus the
in-channel commands the bot responds to.

Credits
-------

Bot logo based on 'dj by Piotrek Chuchla' from the Noun Project,
<https://thenounproject.com/term/dj/21539/>

The DiscordGo library, <https://bwmarrin.github.io/discordgo/>,
and the very helpful community in their Discord channels.