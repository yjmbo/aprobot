// aprobot

// Functions related to the bot's use of Discord,
// and methods for the channelList struct

package main

import (
	"container/ring"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Entry functions:
func botJoinedServer(s *discordgo.Session, m *discordgo.GuildCreate) {
	// We receive these events when someone uses an invite to add this bot
	// to their server.
	debugMsg("event received, %+v", m)

	// Update the status
	err := s.UpdateStatus(1, *c.stationName+". "+*c.CmdPrefix+"help")
	if err != nil {
		debugMsg("Update of bot status failed, %v", err)
	}
}

func (cl *channelList) discordCommands(s *discordgo.Session, m *discordgo.MessageCreate) {
	debugMsg("received MessageCreate event %+v, %+v", m, m.Message.Content)

	if !ignoreSender(s, m.Message) {
		if strings.HasPrefix(m.Message.Content, *c.CmdPrefix) {
			cmd := m.Message.Content[len(*c.CmdPrefix):] // Strip the prefix
			debugMsg("has our prefix: '%v'", cmd)
			debugMsg("ch pre now %+v", cl)

			// Check for any generic commands
			switch cmd {
			case "help":
				rhelpmsg := "This is %v, GNU AGPL licensed, see %v\n" +
					"A user with the '%v' role can switch announcements" +
					"'%von' or '%voff'.\n" +
					"The last %v tracks will be maintained, older ones " +
					"will be removed.\n" +
					"Announcing tracks from '%v'"
				helpmsg := fmt.Sprintf(rhelpmsg,
					version, versionURL, *c.CmdRole,
					*c.CmdPrefix, *c.CmdPrefix, *c.RingSize,
					*c.stationName)
				_, _ = sendDiscordMsg(s, m.ChannelID, helpmsg)
				return
			}

			// Do not proceed if the message author is not authorised
			if !cmdAuthorised(s, m.Message) {
				userMsg("Unauthorised user %v tried command '%v'",
					m.Author.Username+"#"+m.Author.Discriminator,
					m.Message.Content)
				_, _ = sendDiscordMsg(s, m.ChannelID,
					"Sorry, you are not authorised")
			} else {
				switch cmd {
				case "on":
					// The user wants to switch on announcements in the
					// current channel.
					// Check that we're not already on ...
					debugMsg("trying to add a new channel")
					// Lock the list ...
					cl.m.Lock()
					isonin, _ := cl.isOnIn(m.ChannelID)
					if isonin {
						msg := "We are already on in this channel"
						_, _ = sendDiscordMsg(s, m.ChannelID, msg)
					} else {
						msg := "Switching announcements on in this channel"
						_, _ = sendDiscordMsg(s, m.ChannelID, msg)
						cl.Channels = append(cl.Channels,
							channelItem{m.ChannelID,
								m.Author.Username + "#" + m.Author.Discriminator,
								nil, nil})
						// we're still locked, so the appended item *will*
						// be the last item :-
						currPos := len(cl.Channels) - 1
						cl.Channels[currPos].msgRing = ring.New(*c.RingSize + 1)
						if currTrack != "" {
							sm, _ := sendDiscordMsg(s, m.ChannelID, currTrack)
							cl.Channels[currPos].msgRing.Value = sm
							cl.Channels[currPos].msgRing = cl.Channels[currPos].msgRing.Next()
						}
						newChannel, _ := s.Channel(m.ChannelID)
						newChGuild, _ := s.Guild(newChannel.GuildID)
						userMsg("Added bot to channel '%v','%v',%v by request from %v",
							newChGuild.Name, newChannel.Name, m.ChannelID,
							m.Author.Username+"#"+m.Author.Discriminator)
						writeFileFrom(*c.ChFile, &cl)
					}
					cl.m.Unlock()

				case "off":
					debugMsg("trying to remove a channel")
					// Lock the list ...
					cl.m.Lock()
					isonin, i := cl.isOnIn(m.ChannelID)
					if isonin {
						msg := "Stopping announcements in this channel"
						_, _ = sendDiscordMsg(s, m.ChannelID, msg)
						oldChannel, _ := s.Channel(m.ChannelID)
						oldChGuild, _ := s.Guild(oldChannel.GuildID)
						userMsg("Removed bot from channel '%v','%v',%v by request from %v",
							oldChGuild.Name, oldChannel.Name, m.ChannelID,
							m.Author.Username+"#"+m.Author.Discriminator)
						cl.Channels = append(cl.Channels[:i], cl.Channels[i+1:]...)
						writeFileFrom(*c.ChFile, &cl)
					} else {
						msg := "We are not on in this channel"
						_, _ = sendDiscordMsg(s, m.ChannelID, msg)
					}
					cl.m.Unlock()
				}
				debugMsg("ch post now %+v", cl)
			}
		}
	}
}

// Support functions:
func sendDiscordMsg(s *discordgo.Session, ch string, m string) (sm *discordgo.Message, err error) {
	debugMsg("Sending message '%v' to channel '%v'", m, ch)
	sm, err = s.ChannelMessageSend(ch, m)
	if err != nil {
		debugMsg("Message Send failed, err = '%v'", err)
	} else if sm.Content != m {
		debugMsg("Message sent '%+v' != message delivered '%+v'", m, sm.Content)
	}
	return
}

func cmdAuthorised(s *discordgo.Session, m *discordgo.Message) bool {
	// Default deny
	roleOK := false

	// From the Message coming in, we extract ChannelID and AuthorID
	// ChannelID provides GuildID via session.Channel
	// AuthorID and GuildID provide Roles

	debugMsg("Checking command authorisation for message %+v", m)

	channel, _ := s.Channel(m.ChannelID)
	gRoles, _ := s.GuildRoles(channel.GuildID)
	mbr, _ := s.State.Member(channel.GuildID, m.Author.ID)
	roles := mbr.Roles

	var rnames []string
	for _, r := range roles {
		debugMsg("Checking user role %v", r)

		// Find this role in the list of guild Roles
		for _, g := range gRoles {
			debugMsg("Checking that this role is in guild roles, g=%+v", g.ID)

			if g.ID == r {
				rnames = append(rnames, g.Name)
				if g.Name == *c.CmdRole {
					roleOK = true
					return roleOK // no need to check any further
				}
			}
		}
	}

	debugMsg("roleOK %v", roleOK)
	return roleOK
}

func ignoreSender(s *discordgo.Session, m *discordgo.Message) bool {
	// First, ignore anything said by us:
	if s.State.User.ID == m.Author.ID {
		debugMsg("rejecting messages from myself")
		return true
	}
	// Second, ignore other bots
	if m.Author.Bot {
		debugMsg("rejecting messages from other bots")

		return true
	}
	debugMsg("consider messages from this user...")

	return false
}

func (cl *channelList) msgAll(s *discordgo.Session, mText string, mType msgType) {
	// As a special case, we can delete tmpMsg by sending ""
	debugMsg("mText='%+v',mType=%v", mText, mType)

	for i := range cl.Channels {
		debugMsg("Sending msg '%v', type %v to cl[%v]", mText, mType, i)
		cl.Channels[i].sendMsg(s, mText, mType)
	}
}

func (ci *channelItem) sendMsg(s *discordgo.Session, mText string, mType msgType) {
	// Send a msgType message to a channel we belong to
	debugMsg("mText='%v', mType=%v", mText, mType)

	// Empty messages should only make sense for mt=Temporary
	// ... and for those they mean we're only deleting the old one.
	var sm *discordgo.Message
	var err error

	if mText != "" && mType != Temporary {
		sm, err = sendDiscordMsg(s, ci.ChannelID, mText)
		if err != nil {
			userMsg("Error %v; Failed to send Discord msg '%v' to %v",
				err, sm, ci.ChannelID)
			return // Not a fatal problem, we've logged the error
		}
		if mType == Ring {
			// The Ring buffer is broken
			// I *think* it's because I'm failing to assign .Next() to the
			// right thing, but I thought I was passing references rather than copies.
			debugMsg("Dumping ring '%+v' prior ...", ci.msgRing)
			ci.msgRing.Do(func(p interface{}) { debugMsg("= %+v", p) })
			// We need to delete the message in the oldest part of the ring
			// and then add the new/sent one.
			ci.msgRing.Value = sm
			//nrm := ci.msgRing.Next().Value
			if ci.msgRing.Next().Value != nil {
				// There was an old message in the ring
				nrmv := ci.msgRing.Next().Value.(*discordgo.Message)
				debugMsg("Deleting old ring message '%+v'", nrmv)
				s.ChannelMessageDelete(nrmv.ChannelID, nrmv.ID)
				ci.msgRing.Next().Value = nil
			}
			debugMsg("Dumping ring '%+v' post update ...", ci.msgRing)
			ci.msgRing.Do(func(p interface{}) { debugMsg("= %+v", p) })

			// Move on to the next ring position
			ci.msgRing = ci.msgRing.Next()

			debugMsg("Dumping ring '%+v' post Next() ...", ci.msgRing)
			ci.msgRing.Do(func(p interface{}) { debugMsg("= %+v", p) })
			//debugMsg("Dumping the ring post again ...")
			//ci.msgRing.Do(func(p interface{}) { debugMsg("= %+v", p) })

		}
	} else if mType == Temporary {
		// Delete the old temporary message, if there is one
		if ci.tmpMsg != nil {
			debugMsg("Deleting old temp message '%v'", ci.tmpMsg)
			// ci.ChannelID should be the same as ci.tmpMsg.ChannelID
			// but I can't see any reason here to validate that
			s.ChannelMessageDelete(ci.tmpMsg.ChannelID, ci.tmpMsg.ID)
		}
		// If the new message isn't "", broadcast it
		if mText != "" {
			sm, _ = sendDiscordMsg(s, ci.ChannelID, mText)
			ci.tmpMsg = sm
		}
	}
}

func (cl *channelList) isOnIn(cid string) (bool, int) {
	// See if cid is already a member of our child ChannelIDs
	// Return the index if found. Beware any changes between check and use.
	for i, v := range cl.Channels {
		debugMsg("cl.isOnIn(%+v); %+v", cid, v)

		if v.ChannelID == cid {
			return true, i
		}
	}
	// the second value has no meaning when the first is 'false'
	return false, 0
}
