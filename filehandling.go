// aprobot
// Functions to read/write the persistent storage

package main

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"sync"
)

var filelock sync.Mutex

func readFileInto(path string, v interface{}) (err error) {
	debugMsg("Request to read from file '%v'", path)
	filelock.Lock()
	defer filelock.Unlock()

	f, err := os.Open(path)
	if os.IsNotExist(err) { // If the db file doesn't exist, that's acceptable
		return nil
	} else if err != nil {
		return err
	}
	defer f.Close()

	// Because v is an interface{} we don't really care what the contents will be
	err = json.NewDecoder(f).Decode(v)
	return err
	// deferred f.close and filelock.unlock will happen
}

func writeFileFrom(path string, v interface{}) (err error) {
	debugMsg("Request to write to file '%v'", path)
	filelock.Lock()
	defer filelock.Unlock()

	f, err := os.Create(path)
	if err != nil {
		debugMsg("Unable to create file, '%v'", err)
		return err
	}
	defer f.Close()

	// Serialise the value we've been sent
	// Because v is an interface{}, we don't care what the internat structure is
	b, err := json.MarshalIndent(v, "", "\t")

	// Write the serialised value to the file
	bytes, err := io.Copy(f, bytes.NewReader(b))
	debugMsg("%v bytes written", bytes)
	return err
	// deferred f.close and filelock.unlock will happen
}
