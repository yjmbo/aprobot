Installation
============

aprobot is a single stand-alone executable file, and as such
can be installed anywhere with little issue.

However, it will write out the active channel data to a JSON file,
by default just "channels.json" in the current directory.

Here's documentation to help you run aprobot as a systemd service:

Create a system user
--------------------

```sh
# adduser --system --home /opt/aprobot --disabled-login aprobot
Adding system user `aprobot' (UID 109) ...
Adding new user `aprobot' (UID 109) with group `nogroup' ...
Creating home directory `/opt/aprobot' ...
```

Configure the aprobot service
-----------------------------

Put a copy of the aprobot binary in `/opt/aprobot`

Edit the `aprobot.service` file, and add your token to the ExecStart line.
Add any other command-line flags you want for the program's configuration.
Copy the modified `aprobot.service` file to `/etc/systemd/system`
Enable this new service with
```
# systemctl enable aprobot.service
```

If you have a channels.json file from previous testing that you want to
use, copy that into `/opt/aprobot` too.

Running the aprobot service
---------------------------

```sh
# systemctl start aprobot
# journalctl -u rsc-aprobot
-- Logs begin at Tue 2018-07-10 06:00:37 EDT, end at Thu 2018-07-12 06:03:32 EDT. --
Jul 12 06:03:27 ed systemd[1]: Started RSC Aprobot.
Jul 12 06:03:31 ed aprobot[14430]: 2018/07/12 06:03:31.236243 Ready to accept instructions
Jul 12 06:03:32 ed aprobot[14430]: 2018/07/12 06:03:32.331660 Michael Johnson - Hunt for Immortality (6m42s), finishes at 3304-07-12 10:08:42, goroutine sleeping 5m15s
```
