// aprobot
//
// A Discord Bot that announces the tracks playing from an Airtime or
// Airtime Pro streaming radio station

package main

import (
	"container/ring"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
)

// versionURL holds the release code repository, for AGPL compliance
const version string = "aprobot version 1.1.0"
const versionURL string = "https://gitlab.com/yjmbo/aprobot"

// msgTypes are to help decide when/if to delete previously sent Discord messages
type msgType int

// msgTypes are enumerated:
const (
	Permanent msgType = iota
	Temporary
	Ring
)

// Global scope variables
var debug = flag.Bool("debug", false, "Request debug output")
var noatp = flag.Bool("noatp", false, "Replace Airtime polling with test messages (qv -tdel)")
var progVersion = flag.Bool("version", false,
	"Request program version information")
var s *discordgo.Session
var c config
var currTrack string

type config struct {
	stationName  *string
	stationURL   *string
	errorDelay   *int
	updateDelay  *int
	testDelay    *int
	deadAirCount *int
	BotToken     *string
	DSyncDelay   *int
	CmdPrefix    *string
	CmdRole      *string
	RingSize     *int
	ChFile       *string
	YearOffset   *int
}

type resptype int

const (
	track resptype = iota
	deadAir
	fault
)

type pollResponse struct {
	msg     string
	msgtype resptype
}

// msgRing and tmpMsg are not intended to be serialised into the
// config.ChFile, and are therefore not exportable
type channelItem struct {
	ChannelID string
	UserID    string
	msgRing   *ring.Ring
	tmpMsg    *discordgo.Message
}
type channelList struct {
	Channels []channelItem
	m        sync.Mutex
}

func main() {
	// Enable logging
	log.SetFlags(log.Ldate | log.Lmicroseconds)

	//
	// Start by working out the configuration values for this execution:
	// The default station is Radio Sidewinder
	c.stationName = flag.String("sn",
		"Radio Sidewinder", "Airtime station name")
	c.stationURL = flag.String("api",
		"https://radiosidewinder.airtime.pro/api/live-info-v2",
		"Airtime station information API URL")
	c.updateDelay = flag.Int("ud",
		5, "Airtime poll update delay (seconds)")
	c.errorDelay = flag.Int("ed",
		20, "Airtime poll delay on error (seconds)")
	c.deadAirCount = flag.Int("dac",
		3, "Number of Dead Air events (modulo) before informing the listeners")

	c.BotToken = flag.String("token",
		"", "Discord bot authentication token (required, excluding prefix 'Bot ')")
	c.DSyncDelay = flag.Int("dsync",
		2, "Discord initial sync delay")
	c.CmdPrefix = flag.String("cmd",
		"!", "Prefix for bot commands")
	c.CmdRole = flag.String("cmdrole",
		"cmdaprobot", "Server role allowed to instruct the bot")
	c.YearOffset = flag.Int("yoff",
		0, "Offset the year values - by 1286 for Radio Sidewinder")

	c.ChFile = flag.String("chf",
		"channels.json", "Discord channel data")
	c.RingSize = flag.Int("r",
		5, "Number of old Track messages to leave in channels")

	c.testDelay = flag.Int("tdel",
		5, "Test loop delay (needs -noatp)")

	flag.Parse()

	// Debug CLI flags and config values
	if *debug {
		fmt.Println("CLI flags :")
		flag.VisitAll(func(f *flag.Flag) { fmt.Printf("f: %+v\n", f) })

		fmt.Println("Global config values :")
		cj, _ := json.MarshalIndent(&c, "", " ")
		fmt.Printf("config %+v\n%v\n", c, string(cj))

		fmt.Println("Global variables:")
		fmt.Printf("debug %+v, noatp %+v, progVersion %+v, s %+v, currTrack %+v\n",
			debug, noatp, progVersion, s, currTrack)
	}

	// Early exit if ...
	if *progVersion {
		fmt.Println(version)
		os.Exit(0)
	}
	// Required flag: botToken
	if *c.BotToken == "" {
		fmt.Println("Discord Bot Token is required, use '-token TOKEN_DATA'")
		os.Exit(1)
	}

	//
	// Now connect to Discord and set up the event handlers
	//
	var activeChannels channelList
	// Read the list of channels we're supposed to be active on from file
	readFileInto(*c.ChFile, &activeChannels)
	// We need to add a new empty msgRing to each channel
	for i := range activeChannels.Channels {
		activeChannels.Channels[i].msgRing = ring.New(*c.RingSize + 1)
	}
	debugMsg("channels read, %+v", &activeChannels)

	// Now we're running concurrently, so watch locking
	s, err := discordgo.New("Bot " + *c.BotToken)
	haltIf(err, "Failed to establish a (non-connected) Discord session")
	s.AddHandler(botJoinedServer)
	haltIf(s.Open(), "Failed to connect to Discord using token 'Bot "+
		*c.BotToken+"'")

	// Allow a little time for Discord's status to sync with us
	time.Sleep(time.Duration(*c.DSyncDelay) * time.Second)
	// Announce wakeup
	activeChannels.msgAll(s, fmt.Sprintf("%v has come online, playing %v ...",
		version, *c.stationName), Permanent)

	// Set up all the event handlers and goroutines
	s.AddHandler(activeChannels.discordCommands)
	go interruptHandler(s, &activeChannels)

	userMsg("Ready to accept instructions")

	// Check the Airtime API, or just sit here spinning ...
	if *noatp {
		// Every few seconds, generate a test message.
		for tl := 0; ; tl++ {
			tdel := time.Duration(*c.testDelay) * time.Second
			debugMsg("Testing without Airtime ... sleeping for %v", tdel)
			time.Sleep(tdel)
			testMsg := fmt.Sprintf("Testing track %v: %v", tl, time.Now())
			activeChannels.msgAll(s, testMsg, Ring)
		}
	} else {
		//
		// Time to start getting updates from Airtime
		//
		prch := make(chan pollResponse)
		go pollAPI(prch, c)

		var dac int // count of repeated Dead Air messages
		for {
			// Wait for a message to be received from the pollAPI channel
			presp := <-prch
			debugMsg("main: received pollAPI message '%+v'", presp)
			switch presp.msgtype {
			case track:
				dac = 0
				activeChannels.msgAll(s, "", Temporary) // side-effect: "" => delete, therefore call it delete ?
				// Update global var currTrack, so channel joins
				//  can be updated if they happen
				currTrack = presp.msg
				activeChannels.msgAll(s, presp.msg, Ring)
			case deadAir:
				dac++
				debugMsg("Dead air %v %v", dac, presp.msg)
				if (dac % *c.deadAirCount) == 0 {
					currTrack = ""
					activeChannels.msgAll(s,
						"Dead Air: Recombined Scents for Thargoids (duration unknown)",
						Temporary)
				}
			case fault:
				debugMsg("fault reported")
			default:
				debugMsg("unknown")
			}
		}
	}
}

func interruptHandler(s *discordgo.Session, cl *channelList) {
	debugMsg("Interrupt handler being set up")

	signalChan := make(chan os.Signal)
	readyToExit := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)

	// Listen to signalChan for os.Interrupt ...
	go func() {
		<-signalChan
		debugMsg("Interrupt received")
		// When the signal arrives, start cleanup
		userMsg("Interrupt, cleaning up")
		cl.msgAll(s, "Bot shutdown, goodbye.", Permanent)
		readyToExit <- true
	}()

	// Wait for the cleanup after os.Interrupt
	<-readyToExit
	debugMsg("Interrupt cleanup complete, exiting")

	os.Exit(0)
}
