// aprobot

// Functions to talk to the Airtime API

package main

import (
	"encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"net/http"
	"time"
)

// Only a small subset of the Airtime API JSON are used ...
type stationInfo struct {
	Station struct {
		Time string `json:"schedulerTime"`
		TZ   string `json:"timezone"`
	}
	Tracks struct {
		Current struct {
			Starts string
			Ends   string
			Name   string
		}
	}
}

func parseStationJSON(j []byte) (v stationInfo, err error) {
	err = json.Unmarshal(j, &v)
	return
}
func pollAPI(ch chan pollResponse, c config) {
	var resp pollResponse
	const dfmt string = "2006-01-02 15:04:05"

	var lastStationTime string
	// Keep running this loop for ever
	for {
		// Query the station API
		debugMsg("HTTP GET %v ...", *c.stationURL)

		http, err := http.Get(*c.stationURL)
		if err != nil {
			resp.msgtype = deadAir
			resp.msg = "Unable to access website"
			ch <- resp
			debugMsg("dead air: %v, sleeping %v", resp.msg, *c.errorDelay)

			time.Sleep(time.Duration(*c.errorDelay) * time.Second)
			continue
		}
		// If we receive a response, parse it
		body, _ := ioutil.ReadAll(http.Body)
		data, err := parseStationJSON(body)
		if err != nil {
			resp.msgtype = deadAir
			resp.msg = "Unable to parse response"
			ch <- resp
			debugMsg("dead air: %v, sleeping %v", resp.msg, *c.errorDelay)

			time.Sleep(time.Duration(*c.errorDelay) * time.Second)
			continue
		}
		// If the response is valid JSON, it still might be invalid data
		if !(len(data.Tracks.Current.Name) > 0) {
			resp.msgtype = deadAir
			resp.msg = "No current track data"
			ch <- resp
			debugMsg("dead air: %v, sleeping %v", resp.msg, *c.errorDelay)

			time.Sleep(time.Duration(*c.errorDelay) * time.Second)
			continue
		}
		// Sometimes the API returns the same data as last query,
		// so we'll detect this with the Station.Time value
		if data.Station.Time == lastStationTime {
			resp.msgtype = deadAir
			resp.msg = "API data not updated since last query"
			ch <- resp
			debugMsg("dead air: %v, sleeping %v", resp.msg, *c.errorDelay)

			time.Sleep(time.Duration(*c.errorDelay) * time.Second)
			continue
		}
		// No more known error conditions to check for ...
		debugMsg("good API response")

		lastStationTime = data.Station.Time

		// Use the start and end time of the current track to work out
		// the duration.
		tz, _ := time.LoadLocation(data.Station.TZ)
		startt, _ := time.ParseInLocation(dfmt, data.Tracks.Current.Starts, tz)
		endt, _ := time.ParseInLocation(dfmt, data.Tracks.Current.Ends, tz)
		duration := endt.Sub(startt)
		// compare the duration to the station's own declared
		// current time to work out how long we should wait for the next
		// update.
		stationt, _ := time.ParseInLocation(dfmt, data.Station.Time, tz)
		wait := endt.Sub(stationt)

		resp.msg = fmt.Sprintf("%v (%v), finishes at %v",
			html.UnescapeString(data.Tracks.Current.Name), duration,
			endt.UTC().AddDate(*c.YearOffset, 0, 0).Format(dfmt))
		resp.msgtype = track

		// Time how long the chan send takes (probably µs when working well)
		// If Discord gets stuck, this might take a long time
		startChSend := time.Now()
		ch <- resp
		chDelay := time.Now().Sub(startChSend)
		debugMsg("Took %v to send response", chDelay)
		// Subtract this delay from the expected sleep duration
		realWait := wait - chDelay

		if realWait >= 1 {
			userMsg("%v, next check in %v",
				resp.msg, realWait+time.Duration(*c.updateDelay)*time.Second)
			time.Sleep(realWait + time.Duration(*c.updateDelay)*time.Second)
		} else {
			userMsg("%v, checking immediately", resp.msg)
		}

	}
}
