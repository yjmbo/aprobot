# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-06-13

- Initial release version

## [1.0.1]

- Internal refactoring
  - renamed internal logging functions (#10)
  - replaced a break/label with early return (#4)
  - correct sleep duration in pollAPI (#6)
  - replaced hierarchy of functions for sending Discord messages

## [1.1.0]

- Now generating test 'track' messages when running without Airtime polling