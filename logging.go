// aprobot
// Functions for logging

package main

import (
	"fmt"
	"log"
	"path/filepath"
	"runtime"
)

/*
With command-line flag 'debug', output detailed messages for a developer.
Otherwise, only output messages about external events (HTTP and Discord
	errors), for a sysadmin to consume.
*/

func debugMsg(s string, v ...interface{}) {
	if *debug {
		// Discover which function called us
		pc, filename, line, ok := runtime.Caller(1)
		fname := runtime.FuncForPC(pc).Name()
		prefix := ""
		if ok && fname != "" {
			prefix = fmt.Sprintf("[%v:%v#%v] ",
				fname, filepath.Base(filename), line)
		} else {
			prefix = "[?] "
		}
		log.Printf(prefix+s+"\n", v...)
	}
}

func userMsg(s string, v ...interface{}) {
	log.Printf(s+"\n", v...)
}
