// aprobot
// Error and logging functions

package main

import (
	"os"
)

func haltIf(err error, reason string) {
	// Used when a returned error is expected to be fatal,
	// and we should halt program execution
	if err != nil {
		userMsg("%v, error '%+v'", reason, err)
		os.Exit(1)
	}
}
